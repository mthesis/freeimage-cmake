# freeimage-cmake
CMake for [FreeImage](https://freeimage.sourceforge.io/), only tested on linux. Build like
```
git clone https://github.com/jorabold/freeimage-cmake.git
cd freeimage-cmake
cmake -S . -B build #-DCMAKE_INSTALL_PREFIX=/path/to/installdir
sudo cmake --build build --target install -j`nproc`
```
Set `DCMAKE_INSTALL_PREFIX` if you want to install to a different location than `/usr/local/lib/` 
